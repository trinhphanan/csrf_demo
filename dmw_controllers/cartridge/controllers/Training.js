'use strict';

/**
 * Controller that renders the home page.
 *
 * @module controllers/Home
 */
/*API include*/
var URLUtils = require('dw/web/URLUtils');
var Form = require('~/cartridge/scripts/models/FormModel');
var customObj = require('dw/object/CustomObjectMgr');
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var customModel = app.getModel("Custom").get({});
/**
 * Renders the home page.
 */
function start1(){
	return 5;
}
function createSampleProvinceList(){
	var object =[
				{code:'A',name: 'Province 1'},
				{code:'B',name: 'Province 2'},
				{code:'C',name: 'Province 3'},
				{code:'D',name: 'Province 4'},
				{code:'E',name: 'Province 5'}
			];
	var result = new dw.util.ArrayList();
	for each( var key in object ) {
		result.push({'key' : key.code , 'value' : key.name});
	}
	return result;
}
function start() {
	var currentObj = customObj.getCustomObject('NewsletterSubscrip',request.httpParameterMap.ID.stringValue);
	var b = start1();
	app.getView({
    	profile : {name:'An',sex:'male',age:'18'},
    	married : true,
    	getcustomObj : currentObj,
    	getstart1 : b,
    	param1 : 'param1',
    	param2 : 'param2'
    }).render('demo_controller/demo');
}
function createCustomObj() {
	//var myobj = customModel.createCustomObj(request.httpParameterMap.ID.stringValue);
	/*var objUpdate = {};
	objUpdate.ID = "4";
	objUpdate.firstname = "An";
	objUpdate.lastname = "Trinh";	
	objUpdate.email = "an.trinh@bluecomgroup.com";
	var myobj = customModel.updateCustomObj(objUpdate);*/
	/*app.getView({
		obj : myobj
	}).render('demo_controller/createCustomObjInfo')l*/
	
	/*Begin*/
	var myobj = customModel.getoneCustomObj(request.httpParameterMap.ID.stringValue);
	if(null!=myobj){
		var myForm =  app.getForm('customobj');
		myForm.object.ID.value = myobj.custom.ID;
		myForm.object.firstname.value = myobj.custom.firstname;
		myForm.object.lastname.value = myobj.custom.lastname;
		myForm.object.email.value = myobj.custom.email;
		var provinceList = createSampleProvinceList().iterator();
		myForm.object.province.setOptions(provinceList);
		myForm.object.province.value = myobj.custom.province;
		myForm.object.state.value = myobj.custom.stateCode;
	}
	app.getView({
		ContinueURL: URLUtils.https('Training-DemoSubmitForm')
	}).render('form/customobj');
}
function demosubmitform() {
	 app.getForm('customobj').handleAction({
	        update: function () {
	        	//CSRF
	        	var CSRFProtection = require('dw/web/CSRFProtection');
	        	 if (!CSRFProtection.validateRequest()) { //validate the request
	                 app.getModel('Customer').logout();  //log the customer out
	                 app.getView().render('csrf/csrffailed'); //render an error template
	                 return null;
	             }
	        	var myForm =  app.getForm('customobj');
	        	var objUpdate = {};
	        	objUpdate.ID = myForm.object.ID.value;
	        	objUpdate.firstname = myForm.object.firstname.value;
	        	objUpdate.lastname = myForm.object.lastname.value;	
	        	objUpdate.email = myForm.object.email.value;	
	        	objUpdate.province = myForm.object.province.value;
	        	objUpdate.stateCode = myForm.object.state.value;
	            var myobj = customModel.updateCustomObj(objUpdate);
	            response.redirect(URLUtils.https('Training-CreateCustomObj','ID',objUpdate.ID));
	        },
	        add: function () {
	        	var CSRFProtection = require('dw/web/CSRFProtection');
	        	 if (!CSRFProtection.validateRequest()) { //validate the request
	                 app.getModel('Customer').logout();  //log the customer out
	                 app.getView().render('csrf/csrffailed'); //render an error template
	                 return null;
	             }
	        	var myForm =  app.getForm('customobj');
	        	var objUpdate = {};
	        	objUpdate.ID = myForm.object.ID.value;
	        	objUpdate.firstname = myForm.object.firstname.value;
	        	objUpdate.lastname = myForm.object.lastname.value;	
	        	objUpdate.email = myForm.object.email.value;	
	        	objUpdate.province = myForm.object.province.value;
	        	objUpdate.stateCode = myForm.object.state.value;
	        	var myobj = customModel.createCustomObj(objUpdate);
	        	response.redirect(URLUtils.https('Training-CreateCustomObj','ID',objUpdate.ID));
	        },
	        error: function () {
	            response.redirect(URLUtils.https('Account-EditProfile', 'invalid', 'true'));
	        }
	    });
}
/*
 * Export the publicly available controller methods
 */
/** Renders the home page.
 * @see module:controllers/Home~show */
exports.Start = guard.ensure(['get'], start);
exports.CreateCustomObj = guard.ensure(['get'], createCustomObj);
exports.DemoSubmitForm = guard.ensure(['post', 'https', 'csrf'], demosubmitform);
