'use strict';

var AbstractModel = require('./AbstractModel');
var Site = require('dw/system/Site');
var Template = require('dw/util/Template');

/*API include*/
var customObjMgr = require('dw/object/CustomObjectMgr');
/* Script Modules */
var Transaction = require('dw/system/Transaction');

var tableName = "NewsletterSubscrip";
var obj = null;
var CustomModel = AbstractModel.extend({
    createCustomObj : function(parameter){
    	Transaction.wrap(function(){
    	    if (typeof parameter === 'string') {
    	        obj =  customObjMgr.createCustomObject(tableName, parameter);
    	    } else if (typeof parameter === 'object') {
    	    	try{
    	    		obj =  customObjMgr.createCustomObject(tableName, parameter.ID);
        	    	for (var property in parameter) { 
        	    		obj.custom[property] = parameter[property];
             		}
    	    	}catch(e){
    	    		var n = e;
    	    		var m = "";
    	    	}
    	    	
    	    }	
    	  
    	})
    	return obj;
    },
    removeCustomObj : function(ID){
    	var customObj = this.getoneCustomObj(ID);
		if(null!=customObj){
			customObjMgr.remove(customObj);
		}
    },
    renewCustomObj : function(ID){
		this.removeCustomObj(ID);
		return this.createCustomObj(ID);
    },
    updateCustomObj : function(object){
    	flag = 0;
    	Transaction.begin();
    		try{
    			var newObj = this.renewCustomObj(object.ID);
        		for (var property in object) { 
        			newObj.custom[property] = object[property];
        		}
        		flag = 1;
    		}catch(e){
    			var err = e;
    			Transaction.rollback();
    		}
    	Transaction.commit();
    	return flag;
    }, 
    getoneCustomObj : function(ID){
    	Transaction.wrap(function(){
    		obj = customObjMgr.getCustomObject(tableName, ID);
    	})
    	return obj;
    },
    getallCustomObj : function(){
    	Transaction.wrap(function(){
    		obj = customObjMgr.getAllCustomObjects(tableName);
    	})
    	return obj; 
    }
});
CustomModel.get = function (parameter) {
    return new CustomModel(parameter);
};

/** The Email Model class */
module.exports = CustomModel;
