/**
 * 
 */
function getAndInsertCSRFToken(formid){
    $(document).ready(function(){
      $.ajax(
          {     
            url: '${URLUtils.url("CSRF-TokenGeneration")}', 
            context: document.body,
            dataType: 'json',
            success: function(data, status){
                insertCSRFForm(data, formid);
            },
            error: function(xhr, status, error){
                alert('error' + error);
            }
          }
      );
    });    
}
function insertCSRFForm(csrfjson, formid){
    var csrf_name = csrfjson.csrf_token_name;
    var csrf_value = csrfjson.csrf_token_value;
    var form = document.getElementById(formid);
    var inputfield = document.createElement("input");
    inputfield.type = "text";
    inputfield.name = csrf_name;
    inputfield.value = csrf_value;
    var children = form.children;
    form.insertBefore(inputfield, children[children.length-1]);
}