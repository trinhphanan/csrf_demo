'use strict';

var Status = require('dw/system/Status');

exports.modifyGETResponse = function (product) {
	product.brand = "modifyResponseData_Brand";
	return new Status(Status.OK);
};
