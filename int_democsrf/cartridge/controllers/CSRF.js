'use strict';

var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

function csrfFailed() {
    app.getView().render('csrf/csrffailed');
}
function tokenGeneration () {
    app.getView().render('csrf/csrfgenerateJson');
}


exports.Failed = guard.ensure(['get', 'https'], csrfFailed);
exports.TokenGeneration = guard.ensure(['get', 'https'], tokenGeneration);